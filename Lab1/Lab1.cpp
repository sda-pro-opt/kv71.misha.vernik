#include <iostream>
#include <string>
#include <unordered_map>
#include <sstream>
#include <vector>
#include <fstream>

using namespace std;

struct LexerPart {
	string word;
	int row, col;
	int number;
	LexerPart(int row, int col, int number, string word) : word(word), row(row), col(col), number(number)
	{ }
};

class DFA {
private:
	unordered_map<string, bool> types;

	unordered_map<string, int> mp;
	vector<LexerPart> lexerVector;
	int row = 1;
	int col = 0;

	int SYMBOL_COUNT = 0;
	int SYMNOLS_COUNT = 300;
	int KEY_WORDS_COUNT = 400;
	int CONSTANTS_COUNT = 500;
	int IDENTIFIER_COUNT = 1000;

	int CommentHelper(std::fstream& fin, char& state, int& retflag)
	{
		retflag = 1;
		auto res = Comment(fin, state, row, col);
		if (res.first == 0) {
			errorLine(row, col, "Unclosed comment");
			return -1;
		}
		else
			if (res.first == 1) {
				while (res.second == '(' && res.first == 1) {
					res = Comment(fin, res.second, row, col);
					if (res.first == 0) {
						errorLine(row, col, "Unclosed comment");
						return -1;
					}
				}
				if (res.first == 1 || res.first == 3)
					state = res.second;
			}

		if (res.first == 2) { retflag = 3; return {}; };
		retflag = 0;
		return {};
	}
	int IndetifierHelper(std::fstream& fin, char& state, int& retflag)
	{
		retflag = 1;
		auto state_token = Identifier(fin, state, col);
		state = state_token.first;

		if (mp.find(state_token.second) == mp.end()) mp[state_token.second] = ++IDENTIFIER_COUNT;
		print(row, col - state_token.second.length(), state_token.second);

		if (state == '\n') row++, col = 0;

		int retflag1;
		int retval1 = CommentHelper(fin, state, retflag1);
		if (retflag1 == 3) { retflag = 3;  return 3; };
		if (retflag1 == 1) return retval1;
		if (isalpha(state)) {
			state_token = Identifier(fin, state, col);
			state = state_token.first;

			if (mp.find(state_token.second) == mp.end()) mp[state_token.second] = ++IDENTIFIER_COUNT;
			print(row, col - state_token.second.length(), state_token.second);

			if (state == '\n') row++, col = 0;

		}
	
		if (!isspecialsymbol(state) && !isspace(state)) {
			errorLine(row, col, string(1, state));
			return -1;
		}
		else {

			//CheckSpaces(state, row, col);
			if (isspace(state)) { retflag = 3; return {}; }
			else
				if (isspecialsymbol(state)) print(row, col, string(1, state));
		}
		retflag = 0;
		return {};
	}
public:
	unordered_map<string, bool> GetTypes() const {
		return types;
	}
	vector<LexerPart> GetVector() const {
		return lexerVector;
	}

	void InitMap() {
		for (auto el : { "SIGNAL", "COMPLEX","INTEGER","FLOAT","BLOCKFLOAT", "EXT" }) types[el] = 1;
		for (auto el : { "PROGRAM","PROCEDURE","BEGIN","END","SIGNAL", "COMPLEX","INTEGER","FLOAT","BLOCKFLOAT", "EXT" }) mp[el] = ++KEY_WORDS_COUNT;
		for (auto el : { ".", ";", ",", ":", "(", ")" }) mp[el] = ++SYMBOL_COUNT;
	}

	int Process() {
		InitMap();
		char state;
		fstream fin("Tests/test1", fstream::in);
		while (fin >> noskipws >> state) {
			col++;
			if (state == '\n') row++, col = 0;
			if (isspace(state)) continue;


			int retflag;
			int retval = CommentHelper(fin, state, retflag);
			if (retflag == 3) continue;
			if (retflag == 1) return retval;


			auto date = Date(fin, state, col);
			if (date.first == -1) {
				errorLine(row, col, date.second); return -1;
			}
			else if (date.first == 1) {
				if (mp.find(date.second) == mp.end()) mp[date.second] = ++IDENTIFIER_COUNT;
				print(row, col - date.second.length() + 1, date.second);
				continue;
			}

			if (isalpha(state)) {
				int retflag;
				int retval = IndetifierHelper(fin, state, retflag);
				if (retflag == 3) continue;
				if (retflag == 1) return retval;
			}
			else
				if (isdigit(state)) { errorLine(row, col, string(1, state)); return -1; }
				else
					if (isspace(state)) {
						if (state == '\n') row++, col = 0;

						continue;
					}
					else
						if (isspecialsymbol(state)) print(row, col, string(1, state));
						else { errorLine(row, col, string(1, state)); return -1; }
		}
		return 0;
	}


	pair<int, string> Date(fstream& fin, char state, int& col) {
		char ch = state;
		vector<char> v;
		string year = "";
		string month = "";
		string day = "";
		if (state == '#') {
			/*
				1. 9999
				2. -
				3. 99
				4. -
				5. 99

			*/
			string ret = "";
			for (int i = 0; i < 4; i++) {
				fin >> noskipws >> ch; col++;

				if (isdigit(ch)) {
					year += ch;
				}
				else
					return { -1, string(1, ch) };
			}
			int _year = atoi(year.c_str());
			if (_year >= 1960 && _year <= 2100) {

			}
			else {
				return { -1, "Wrong Year < 1960  or > 2100" };
			}
			fin >> noskipws >> ch;
			col++;
			if (ch == '-') {

			}
			else
				return { -1, string(1, ch) };

			for (int i = 0; i < 2; i++) {
				fin >> noskipws >> ch;
				col++;
				if (isdigit(ch)) {
					month += ch;
				}
				else
					return { -1, string(1, ch) };
			}
			int _month = atoi(month.c_str());
			if (_month >= 1 && _month <= 12) {

			}
			else {
				return { -1, "Wrong Month < 01  or > 12" };
			}
			fin >> noskipws >> ch;
			col++;
			if (ch == '-') {

			}
			else
				return { -1, string(1, ch) };

			for (int i = 0; i < 2; i++) {
				fin >> noskipws >> ch;
				col++;
				if (isdigit(ch)) {
					day += ch;
				}
				else
					return { -1, string(1, ch) };;
			}



			int _day = atoi(day.c_str());
			if (_day >= 1 && _day <= 31) {

			}
			else {
				return { -1, "Wrong Day < 01  or > 31" };
			}
			if ((_year >= 1960 && _year <= 2100) && (_month >= 1 && _month <= 12) && (_day >= 1 && _day <= 31)) {
				return { 1, "#" + year + "-" + month + "-" + day };
			}
			else
				return { -1, " " };
		}
		return { 0, " " };
	}

	pair<char, string> Identifier(fstream& fin, char state, int& col) {
		if (isdigit(state)) return {};
		char ch = state;
		string token;

		while ((isalpha(ch) || isdigit(ch)) && !fin.eof()) {
			token.push_back(ch);
			fin >> noskipws >> ch;
			col++;
		}

		return { ch, token };
	}

	pair<int, char> Comment(fstream& fin, char state, int& row, int& col) {
		int tmpCol = col;
		int tmpRow = row;
		if (state == '(') {
			char ch = state;
			fin >> noskipws >> ch; col++; if (ch == '\n') row++, col = 0;
			if (ch == '*') {
				char prevState = ' ';
				fin >> noskipws >> ch; col++; if (ch == '\n') row++, col = 0;


				while ((prevState != '*' || ch != ')') && !fin.eof()) {
					prevState = ch;
					fin >> noskipws >> ch; col++; if (ch == '\n') row++, col = 0;
				}

				if (prevState != '*' || ch != ')')
					return { 0, ' ' };
				return { 2, ' ' };
			}
			else {
				print(tmpRow, tmpCol, string(1, state));
				return { 1, ch };
			}
		}
		else {
			return { 3, state };
		}
	}


	void CheckSpaces(char ch, int& row, int& col)
	{
		if (ch == '\n') row++, col = 0;
		else col++;
	}

	bool isspecialsymbol(char ch) {
		if (string(";,.():").find(ch) != string::npos) return 1;
		return 0;
	}

	void print(int row, int col, string str) {
		lexerVector.push_back(LexerPart(row, col, mp[str], str));
		//cout << row << " " << col << " " << mp[str] << " " << str << "\n";
	}

	void errorLine(int row, int col, string word) {
		cout << "Lexer: Error(line: " << row << ", column: " << col << ") Illegal Symbol: " << word << "\n";
	}
};


class Parser {
private:
	vector<LexerPart> input;
	unordered_map<string, bool> types;
public:
	Parser(vector<LexerPart> input, unordered_map<string, bool> types) : input(input), types(types) {}

	void Error(int index, string expectedWord) {
		cout << "Parser: Error(line: " << input[index].row << ", column: " << input[index].col << ") found KeyWord: " << input[index].word << "\nExpected : " << expectedWord << "\n";
	}

	void TabIndex(int tabs) {
		for (int i = 0; i < tabs; i++) {
			cout << "\t";
		}
	}

	int Program(int& index, int& tabIndex) {
		int tmp = tabIndex;
		if (input[index].word == "PROCEDURE") {
			TabIndex(tabIndex); tabIndex++;
			cout << "<program>\n";
		//	TabIndex(tabIndex);
		//	cout << input[index].number << " " << input[index].word << "\n";
		//	index++;
			int p = Procedure(index, tabIndex);
			if (p != -1) {
				tmp = tabIndex;
				ProcedureIdentifier(index, tabIndex);
				tabIndex = tmp;
				ParametersList(index, tabIndex);
				SemiColumn(index, tabIndex);
				Block(index, tabIndex);
				SemiColumn(index, tabIndex);
				if (input.size() >= index + 1) {
					Error(index, "END OF SIGNAL PROGRAM");
					exit(0);
				}
			}
		}else
		if (input[index].word == "PROGRAM") {
			// PROGRAM <procedure-identifier> ;  
			TabIndex(tabIndex); tabIndex++;
			cout << "<program>\n";
			TabIndex(tabIndex); 
			cout << input[index].number << " " << input[index].word << "\n";
			index++;			
			tmp = tabIndex;
			ProcedureIdentifier(index, tabIndex);			
			tabIndex = tmp;
			SemiColumn(index, tabIndex);
			// <block>. | PROCEDURE <procedure-identifier><parameters-list> ; <block> ; 			
			int b = Block(index, tabIndex);
			if (b == 1) {				
				Dot(index, tabIndex);
			}
			int p = -1;
			if (b == -1) {
				 
				
			}
			if (b == -1 && p == -1) {
				Error(index, "<block> | PROCEDURE");
				exit(0);
			}
		}
		else {
			Error(index, "PROGRAM");
			exit(0);
		}

		return 1;
	}

	void SignalProgram(int index, int tabIndex) {
		cout << "<signal-program>\n";
		Program(index, tabIndex);
	}

	int Block(int& index, int& tabIndex) {
		int tmp = tabIndex;
		if (input[index].word == "BEGIN") {
			TabIndex(tabIndex); tabIndex++;
			cout << "<block>\n";
			TabIndex(tabIndex);  
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
			
			StatementsList(index, tabIndex);
			tabIndex = tmp + 1;
			if (input[index].word == "END") {
				TabIndex(tabIndex);  
				cout << input[index].number << " " << input[index].word << "\n";
				index++;
				tabIndex--;
			}
			else {
				Error(index, "END");
				exit(0);
			}
		}
		else {
			// Error(index, "BEGIN");
			// exit(0);
			return -1;
		}
		return 1;
	}

	int ProcedureIdentifier(int& index, int& tabIndex) {
		if (input[index].number >= 1000) {
			TabIndex(tabIndex); tabIndex++;
			cout << "<procedure-identifier>\n";
			TabIndex(tabIndex); tabIndex++;
			cout << "<identifier>\n";
			TabIndex(tabIndex);  tabIndex++;
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
			
		}
		else {
			Error(index, "<procedure-identifier>");
			exit(0);
		}

		return 1;
	}

	int ParametersList(int& index, int& tabIndex) {
	
		if (input[index].word == "(") {			
			TabIndex(tabIndex); tabIndex++; 
			int tmp = tabIndex;
			cout << "<parameters-list>\n";  
			TabIndex(tabIndex); tabIndex++;
			cout << input[index].number << " " << input[index].word << "\n";			
			index++;
			tabIndex = tmp;
			DeclarationList(index, tabIndex);
			tabIndex = tmp ;
			if (input[index].word == ")") {
				TabIndex(tabIndex); tabIndex--;
				cout << input[index].number << " " << input[index].word << "\n";
				index++;				
			}
			else {
				Error(index, ")");
				exit(0);
			}
		}
		else {
			// Error(index, "(");
			// exit(0);
			
			TabIndex(tabIndex);
			cout << "<parameters-list>\n";
			tabIndex++;
			Empty(tabIndex);
			tabIndex--;
			// SemiColumn(index, tabIndex);
		}
		return 1;
	}

	void ProcedureList() {

	}

	int Procedure(int& index, int& tabIndex) {
		if (input[index].word == "PROCEDURE") {
			TabIndex(tabIndex);
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
		}
		else {
			// Error(index, "PROCEDURE");
			// exit(0);
			return -1;
		}
		return 1;
	}

	int SemiColumn(int& index, int& tabIndex) {
		if (input[index].word == ";") {
			TabIndex(tabIndex);  
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
		}
		else {
			Error(index, ";");
			exit(0);
		}

		return 1;
	}
	int Dot(int& index, int& tabIndex) {
		if (input[index].word == "." && index + 1 >= input.size()) {
			TabIndex(tabIndex);
			cout << input[index].number << " " << input[index].word << "\n";
			// end of program			
		}
		else {
			Error(index, ".");
			exit(0);
		}

		return 1;
	}
	int StatementsList(int& index, int& tabIndex) {
		if (input[index].word == "END") {
			//tabIndex++;
			TabIndex(tabIndex); tabIndex++;
			cout << "<statements-list>\n";
			Empty(tabIndex);
		}
		else {
			Error(index, "END");
			exit(0);
		}

		return 1;
	}

	int DeclarationList(int& index, int& tabIndex) {
		int tmp = tabIndex;
		// <declarations-list> --> <declaration> <declarations-list> | <empty> 
		while (index < input.size() && input[index].number >= 1000 ) {
			
			TabIndex(tabIndex); tabIndex++;
			cout << "<declaration-list>\n";
			Declaration(index, tabIndex);
			//tabIndex = tmp + 1;
			tabIndex -= 1;
		}
		//tabIndex = tmp + 2;
		
		TabIndex(tabIndex); tabIndex++;
		cout << "<declaration-list>\n";  
		Empty(tabIndex);
		tabIndex = tmp;

		return 1;
	}

	int Coma(int& index, int& tabIndex) {
		if (input[index].word == ",") {
			TabIndex(tabIndex);  
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
		}
		else {
			return -1;
		}
		return 1;
	}

	int Declaration(int& index, int& tabIndex) {
		// <declaration> --><variable-identifier><identifiers-list>:<attribute><attributes-list> ; 
		TabIndex(tabIndex); tabIndex++;
		cout << "<declaration>\n";
		VariableIdentifier(index, tabIndex);
		IdentifiersList(index, tabIndex);		
		Colon(index, tabIndex);
		Attribute(index, tabIndex);
		AttributesList(index, tabIndex);
		SemiColumn(index, tabIndex);

		return 1;
	}

	int Colon(int& index, int& tabIndex)
	{
		if (input[index].word == ":") {
			
			TabIndex(tabIndex); 
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
		}
		else {
			Error(index, ":");
			exit(0);
		}
		return 1;
	}

	void Empty(int& tabIndex) {
		TabIndex(tabIndex);
		cout << "<empty>\n";
	 
	}
	int VariableIdentifier(int& index, int& tabIndex) {
		if (input[index].number >= 1000) {
			TabIndex(tabIndex); tabIndex++;
			cout << "<variable-identifier>\n";
			TabIndex(tabIndex); tabIndex++;
			cout << "<identifier>\n";
			TabIndex(tabIndex);  tabIndex++;
			cout << input[index].number << " " << input[index].word << "\n";
			index++;

			tabIndex -= 3;
		}
		else {
			Error(index, "<variable-identifier>");
			exit(0);
		}

		return 1;
	}
	int IdentifiersList(int& index, int& tabIndex) {
		// <identifiers-list> --> , <variable-identifier> <identifiers-list> | <empty> 	 
		int t1 = tabIndex;
		while (input[index].word == ",") {	
			
			TabIndex(tabIndex); tabIndex++;
			cout << "<identifiers-list>\n";		 
			TabIndex(tabIndex);  
			cout << input[index].number << " " << input[index].word << "\n";
			index++;			
			int tmp = tabIndex;
			VariableIdentifier(index, tabIndex);			 
			tabIndex = tmp;
		}	 
		
		TabIndex(tabIndex); tabIndex++;
		cout << "<identifiers-list>\n";
		Empty(tabIndex); // fix tabs	
		tabIndex = t1;
		return 1;
	}

	int Attribute(int& index, int& tabIndex) {
		if (types.find(input[index].word) != types.end()) {
			TabIndex(tabIndex);  tabIndex++;
			cout << "<attribute>\n";
			TabIndex(tabIndex); 
			cout << input[index].number << " " << input[index].word << "\n";
			index++;
			tabIndex--;
		}
		else {
			Error(index, "<attribute>");
			exit(0);
		}
		return 1;
	}

	void AttributesList(int& index, int& tabIndex) {
		bool t = false;

		int t1 = tabIndex;
		TabIndex(tabIndex);  
		cout << "<attributes-list>\n";
		if (types.find(input[index].word) != types.end()) {			
			if (!t) tabIndex++;
			t = true;
			Attribute(index, tabIndex);
			tabIndex--;
			while (types.find(input[index].word) != types.end()) {
				int tmp = tabIndex; 
				tabIndex++;
				TabIndex(tabIndex); 
				cout << "<attributes-list>\n";
				tabIndex++;
				Attribute(index, tabIndex); 
				tabIndex = tmp + 1;
				//tabIndex = tmp;
				//tabIndex--;
				
			}
			
		}

		if (t) {
			tabIndex++;
			TabIndex(tabIndex);
			cout << "<attributes-list>\n";
			tabIndex++;
		}
		else tabIndex++;
		Empty(tabIndex);
		tabIndex = t1;
	}

	void Identifier() {

	}

	int Process() {
		int i = 0, j = 1;
		SignalProgram(i, j);

		return 1;
	}
};

int main() {
	DFA s;
	int t = s.Process();

	Parser p(s.GetVector(), s.GetTypes());
	p.Process();
	return t;
}

